package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.FileFilter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

public class Controller {

    @FXML public Button procurarButton;
    @FXML public TextField diasTextField;
    @FXML public TextArea listaTextArea;


    @FXML
    public void initialize() {

    }

    @FXML
    public void listarDocumentos() {

        String myPath = "C:\\Users\\Guilerme";
        File baseDir = new File(myPath);

        long epoch = LocalDateTime.now().minusDays(5).
                atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

        File[] files = baseDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.lastModified() >= epoch;
            }
        });

        ObservableList<String> listas = FXCollections.observableArrayList();

        listas.addAll(String.valueOf(files));
        listaTextArea.appendText(String.valueOf(listas).toString()+"\n");

        System.out.println(Arrays.toString(files));
    }
    }

